from proxy_info import config
import random

def get_headers_proxy() -> dict:
    try:
        users = config.USER_AGENTS_PROXY_LIST
        persona = random.choice(users)
    except ImportError:
        persona = None
    return persona

def main():
    persona = get_headers_proxy()
    options_proxy = {
        'proxy': {
            'http': persona['http_proxy'],
            'no_proxy': 'localhost,127.0.0.1:8080'
        }
    }
    print(persona['http_proxy'])


if __name__ == '__main__':
    main()